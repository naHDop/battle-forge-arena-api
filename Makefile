.PHONY: all

run:
	PYTHONPATH=app python3 main.py

.PHONY: init-alembic
init-alembic:
	alembic init alembic

.PHONY: migration-add-auto
migration-new-auto:
	alembic revision --autogenerate -m "$(name)"

.PHONY: migration-up
migration-up:
	alembic upgrade head

.PHONY: migration-down
migration-down:
	alembic downgrade -1

.PHONY: migration-erase
migration-erase:
	alembic downgrade base