# Battle Forge Arena

*install 'make' if you have windows*

`choco install make`

### Install 'mini' conda

[install conda](https://docs.anaconda.com/free/miniconda/miniconda-install/)

### Create virtual env

`conda env create --prefix ./env -f environment.yml`

### Find and activate just created virtual env

`conda env list`

`conda activate <full path to conda env>`

### Run the application

`make run`

### Create migration file

`make migration-new-auto name=<file_name>`

### Run Up migration

`make migration-up`

### Run Down migration

`make migration-down`