if __name__ == "__main__":
    import uvicorn
    from app.settings import settings

    uvicorn.run(
        "app.app:app",
        host=settings.host,
        port=settings.port,
        reload=settings.env.is_local(),
    )