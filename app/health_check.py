import logging

from fastapi import APIRouter
from sqlalchemy import text

from app.schemas.base import BaseResponse
from db.session_dependency_middleware import DBSessionDep


router = APIRouter(tags=["health_check"])
logger = logging.getLogger(__name__)


@router.get("/liveless", response_model=BaseResponse[str])
async def liveless():
    return BaseResponse(data="Ok")


@router.get("/readiness", response_model=BaseResponse[str])
async def readiness(db: DBSessionDep):
    try:
        await db.execute(text("select 1"))
        return BaseResponse(data="Ok")
    except Exception as e:
        logger.exception(f"Database not ready: %s", e)
        res = BaseResponse()
        res.error = e
        return res
