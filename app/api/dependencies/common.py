from fastapi import FastAPI
from contextlib import asynccontextmanager
from typing import Callable, TypeVar

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.session import sessionmanager
from app.db.session_dependency_middleware import DBSessionDep

T = TypeVar("T")


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Function that handles startup and shutdown events.
    To understand more, read https://fastapi.tiangolo.com/advanced/events/
    """
    yield
    if sessionmanager._engine is not None:
        # Close the DB connection
        await sessionmanager.close()


def get_repo(repo: Callable[[AsyncSession], T]) -> Callable[[AsyncSession], T]:
    def _get(session: DBSessionDep) -> T:
        return repo(session)

    return _get
