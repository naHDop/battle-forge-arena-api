import os
from dotenv import load_dotenv
from pydantic import BaseModel
from typing import List

load_dotenv(".env")


class DB(BaseModel):
    host: str = os.getenv("DB_HOST")
    port: int = int(os.getenv("DB_PORT"))
    user: str = os.getenv("DB_USER")
    password: str = os.getenv("DB_PASSWORD")
    name: str = os.getenv("DB_NAME")

    def get_dsn(self):
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.name}"


class Environment(BaseModel):
    name: str = os.getenv("ENV")

    def is_local(self):
        return self.name == "local"


class Cors(BaseModel):
    headers: List[str] = os.getenv("ALLOWED_HEADERS").split(",")
    methods: List[str] = os.getenv("ALLOWED_METHODS").split(",")
    origins: List[str] = os.getenv("ALLOWED_ORIGIN").split(",")


class Oauth(BaseModel):
    client_secret: List[str] = os.getenv("GOOGLE_OAUTH2_CLIENT_SECRET")
    client_id: List[str] = os.getenv("GOOGLE_OAUTH2_CLIENT_ID")


class Settings(BaseModel):
    env: Environment = Environment()
    db: DB = DB()
    cors: Cors = Cors()
    oauth: Oauth = Oauth()
    host: str = os.getenv("HOST")
    port: int = int(os.getenv("PORT", 8080))


settings = Settings()
