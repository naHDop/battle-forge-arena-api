from fastapi import APIRouter, Request

from app.db.models.user import User
from app.schemas.base import BaseResponse
from app.schemas.user.user import UserDto


router = APIRouter()


@router.get("/profile", response_model=BaseResponse[UserDto])
async def read_profile(request: Request):
    request_user: User = request.state.user
    dto = _adapt_to_user_process_model(request_user)

    return BaseResponse(data=dto)


def _adapt_to_user_process_model(in_: User) -> UserDto:
    return UserDto(
        id=in_.id,
        email=in_.email,
        first_name=in_.first_name,
        last_name=in_.last_name,
        img_src=in_.img_src,
        is_verified=in_.is_verified
    )