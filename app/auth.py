import httpx
import logging

from fastapi import HTTPException, Request
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import JSONResponse
from settings import settings
from app.db.session import get_db_session
from app.db.models import User
from app.db.repositories.user_repository import UserRepository
from cachetools import TTLCache

logger = logging.getLogger(__name__)

GOOGLE_TOKEN_INFO_URL = "https://oauth2.googleapis.com/tokeninfo"

# Create a TTLCache with a max size of 1000 items and a TTL of 1 hour
token_cache = TTLCache(maxsize=1000, ttl=60 * 60)


async def get_user_from_token(token: str, user_repository: UserRepository):
    if token in token_cache:
        return token_cache[token]

    async with httpx.AsyncClient() as client:
        response = await client.get(GOOGLE_TOKEN_INFO_URL, params={"id_token": token})
        if response.status_code != 200:
            raise HTTPException(status_code=401, detail="Invalid token")

        token_info = response.json()
        # if token_info['aud'] != "407408718192.apps.googleusercontent.com":
        if token_info['aud'] != settings.oauth.client_id:
            raise HTTPException(status_code=401, detail="Invalid audience")

        email = token_info["email"]
        first_name = token_info.get("given_name")
        last_name = token_info.get("family_name")
        picture = token_info.get("picture")

        user = await user_repository.get_user_by_email(email)
        if not user:
            user = User(email=email, first_name=first_name, last_name=last_name, img_src=picture, is_verified=True)
            user = await user_repository.add_user(user)

        token_cache[token] = user
        return user


class GoogleAuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        token = request.headers.get("Authorization")
        if not token or not token.startswith("Bearer "):
            return JSONResponse(status_code=401, content={"error": "Not authenticated", "data": None})

        token = token[len("Bearer "):]

        async for db in get_db_session():
            user_repo = UserRepository(db)
            try:
                user = await get_user_from_token(token, user_repo)
            except HTTPException as e:
                logger.exception(e)
                return JSONResponse(status_code=e.status_code, content={"error": e.detail, "data": None})

            request.state.user = user
            response = await call_next(request)
            return response
