from sqlalchemy import UUID
from sqlalchemy.orm import Mapped, mapped_column
from uuid import uuid4, UUID as pyUUID

from . import Base


class User(Base):
    __tablename__ = "users"

    id: Mapped[pyUUID] = mapped_column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    email: Mapped[str] = mapped_column(index=True, unique=True)
    first_name: Mapped[str] = mapped_column(nullable=True)
    last_name: Mapped[str] = mapped_column(nullable=True)
    img_src: Mapped[str] = mapped_column(nullable=True)
    is_verified: Mapped[bool] = mapped_column(default=False)