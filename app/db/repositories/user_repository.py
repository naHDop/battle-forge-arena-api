from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from app.db.models.user import User


class UserRepository:
    def __init__(self, db: AsyncSession):
        self.db = db

    async def get_user_by_email(self, email: str) -> User | None:
        curs = await self.db.execute(
            select(User).where(User.email == email)
        )
        return curs.scalar_one_or_none()

    async def get_user_by_id(self, user_id: UUID) -> User | None:
        curs = await self.db.execute(
            select(User).where(User.id == user_id)
        )
        return curs.scalar_one_or_none()

    async def add_user(self, user: User):
        self.db.add(user)
        await self.db.commit()
        await self.db.refresh(user)
        return user


__all__ = ["UserRepository"]