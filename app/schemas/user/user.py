from uuid import UUID

from app.schemas.base import BaseApiModel


class UserDto(BaseApiModel):
    id: UUID
    email: str
    first_name: str | None = None
    last_name: str | None = None
    img_src: str | None = None
    is_verified: bool | None = True
