from app.schemas.base import BaseApiRequest


class CreateUserDto(BaseApiRequest):
    email: str
    first_name: str | None = None
    last_name: str | None = None
    img_src: str | None = None
    is_verified: bool | None = True
