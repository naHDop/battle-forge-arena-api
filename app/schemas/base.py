from typing import Generic, TypeVar

from pydantic import BaseModel, ConfigDict
from pydantic.alias_generators import to_camel

T = TypeVar("T")


class BaseResponse(BaseModel, Generic[T]):
    data: T | None = None
    error: str | None = None


class BaseApiModel(BaseModel):

    model_config = ConfigDict(
        alias_generator=to_camel,
        from_attributes=True,
        populate_by_name=True,
        extra="forbid",
    )


class BaseApiRequest(BaseModel):
    model_config = ConfigDict(alias_generator=to_camel, extra="forbid")