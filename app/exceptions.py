class AppException(Exception):
    pass


class AccessDenied(AppException):
    pass


class BadRequest(AppException):
    pass
