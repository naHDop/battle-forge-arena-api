import logging
from exceptions import AccessDenied, BadRequest

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse

from settings import settings
from app.auth import GoogleAuthMiddleware
from app.schemas.base import BaseResponse
from app.api.dependencies import common

from app.health_check import router as health_check_router
from app.user import router as user_router


logger = logging.getLogger(__name__)

app = FastAPI(lifespan=common.lifespan)
app.include_router(health_check_router, prefix="/health")
app.include_router(user_router, prefix="/api")


@app.exception_handler(Exception)
async def unicorn_exception_handler(request: Request, exc: Exception):
    if isinstance(exc, AccessDenied):
        return JSONResponse(
            status_code=status.HTTP_403_FORBIDDEN,
            content=BaseResponse(
                error=str(exc),
                data=None,
            ).model_dump(),
        )
    if isinstance(exc, BadRequest):
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content=BaseResponse(
                error=str(exc),
                data=None,
            ).model_dump(),
        )
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content=BaseResponse(
            error=str(exc),
            data=None,
        ).model_dump(),
    )

app.add_middleware(GoogleAuthMiddleware)
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.cors.origins,
    allow_credentials=True,
    allow_methods=settings.cors.methods,
    allow_headers=settings.cors.headers,
)
